# ORID
## O
- Code review: In this morning's code review, I was selected by the group to explain the code I wrote yesterday. When I showcase my code in public, I feel a bit under pressure. When it is pointed out that the code has flaws, I need to modify it on-site. 
- Unit Test: This morning we also learned about unit testing. As the name suggests, unit testing can test whether some of our code can achieve the expected results. The usage of unit testing can be summarized into four steps, namely describing functions, listing conditions, executing goals, and verifying results. When coding in the past, I rarely used unit testing, which was basically verified through the printed information of functions. 
- TDD: TDD is also known as Test-driven development. It requires writing test cases before coding to promote the whole development through testing. 
## R
Good
## I
Experience of being selected by a group to review code left a deep impression on me, which allowed me to learn and remember more coding techniques.

I found that using unit testing was actually more convenient because it could avoid situations where the results were originally correct, but after modifying some of the code, the results were incorrect.

I think TDD is a good development method. It can make us clear the target requirements, find some errors as soon as possible, and reduce the cost of Code refactoring.
## D
When writing code in the future, I will try using the TDD to write code instead of just like before.
