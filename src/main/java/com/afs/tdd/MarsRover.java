package com.afs.tdd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MarsRover {
    private Location location;
    private List<Action> actionList;
    public MarsRover(Location location) {
        this.location = location;
        actionList = new ArrayList<>();
    }

    public Location executeCommand(Command command) {
        if (command.equals(Command.Move)){
            MoveAction moveAction = new MoveAction();
            this.addAction(moveAction);
        }
        if (command.equals(Command.TurnLeft)){
            TurnLeftAction turnLeftAction = new TurnLeftAction();
            this.addAction(turnLeftAction);
        }
        if (command.equals(Command.TurnRight)){
            TurnRightAction turnRightAction = new TurnRightAction();
            this.addAction(turnRightAction);
        }
        return this.executeAction();
    }
    public Location executeBatchCommand(String command) {

        HashMap<String, Command> commandMap = new HashMap<>();
        commandMap.put("M",Command.Move);
        commandMap.put("L",Command.TurnLeft);
        commandMap.put("R",Command.TurnRight);

        List<String> result = Arrays.asList(command.split(""));
        result.stream()
                .map(commandMap::get)
                .forEach(this::executeCommand);
        return this.location;
    }
    public void addAction(Action action){
        this.actionList.add(action);
    }
    public Location executeAction() {
        for (Action action:this.actionList){
            action.execute(this.location);
        }
        actionList.clear();
        return this.location;
    }
}