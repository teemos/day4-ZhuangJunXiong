package com.afs.tdd;

public class TurnRightAction implements Action {
    @Override
    public void execute(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.East);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case East:
                location.setDirection(Direction.South);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }
    }
}