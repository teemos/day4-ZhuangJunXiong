package com.afs.tdd;

public class MoveAction implements Action {
    @Override
    public void execute(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setCoordinateY(location.getCoordinateY() + 1);
                break;
            case South:
                location.setCoordinateY(location.getCoordinateY() - 1);
                break;
            case East:
                location.setCoordinateX(location.getCoordinateX() + 1);
                break;
            case West:
                location.setCoordinateX(location.getCoordinateX() - 1);
                break;
        }
    }
}