package com.afs.tdd;

public enum Direction {
    North, West, East, South
}
