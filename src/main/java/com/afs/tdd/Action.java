package com.afs.tdd;

public interface Action {
    void execute(Location location);
}
