package com.afs.tdd;

public class TurnLeftAction implements Action {
    @Override
    public void execute(Location location) {
        switch (location.getDirection()) {
            case North:
                location.setDirection(Direction.West);
                break;
            case South:
                location.setDirection(Direction.East);
                break;
            case East:
                location.setDirection(Direction.North);
                break;
            case West:
                location.setDirection(Direction.South);
                break;
        }
    }
}