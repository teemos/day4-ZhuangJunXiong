package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {
    // 0,0,N
    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_and_command_move() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_N_and_command_turnLeft() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnLeft;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_N_and_command_turnRight() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnRight;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    // 0,0,S
    @Test
    void should_change_to_0_Minus_1_S_when_executeCommand_given_location_0_0_S_and_command_move() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_S_and_command_turnLeft() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnLeft;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_S_and_command_turnRight() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnRight;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }
    // 0,0,E
    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_and_command_move() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_E_and_command_turnLeft() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnLeft;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_E_and_command_turnRight() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnRight;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }
    // 0,0,W
    @Test
    void should_change_to_Minus_1_0_W_when_executeCommand_given_location_0_0_W_and_command_move() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(-1, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_W_and_command_turnLeft() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnLeft;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_W_and_command_turnRight() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.TurnRight;
        // When
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        // Then
        Assertions.assertEquals(0, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_3_2_S_when_executeBatchCommand_given_location_0_0_N_and_command_String() {
        // Given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        String commandMove = "MRLRMMLMRMR";
        // When
        Location locationAfterMove = marsRover.executeBatchCommand(commandMove);
        // Then
        Assertions.assertEquals(3, locationAfterMove.getCoordinateX());
        Assertions.assertEquals(2, locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }
}
